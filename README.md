# Supplementary material archive
Code and data for the submitted manuscript _Privacy-preserving generalized linear models using distributed block coordinate descent_

1. [`privreg-images`](privreg-images) holds standalone R-scripts for generating two of the figures in the manuscript
3. [`privreg-simulations`](privreg-simulations) contains all the code for the comparison of our privacy-preserving GLM method to full-data GLM using monte carlo simulations.
2. [`privreg-experiments`](privreg-experiments) holds all the preprocessing and analysis code for the privacy-preserving GLM analyses on three datasets from the UCI machine learning repository. The raw data can additionally be found on the UCI website here:
   1. [Forest fires](https://archive.ics.uci.edu/ml/datasets/forest+fires)
   2. [Hepatocellular carcinoma](https://archive.ics.uci.edu/ml/datasets/HCC+Survival)
   3. [Diabetes](https://archive.ics.uci.edu/ml/datasets/diabetes)
   
I suggest using the `.Rproj` files to open the folders as an `R` project in `RStudio`, where possible.
